import QtQuick 2.9
import QtQuick.Controls 2.2
import QtWebEngine 1.7
import Lomiri.Components 1.3 as Lomiri
import Morph.Web 0.1

ApplicationWindow {
    id: root

    visible: true
    width: units.gu(50)
    height: units.gu(75)
    color: '#43286A'

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu 20.04 like Android 9) AppleWebKit/537.36 Chrome/87.0.4280.144 Mobile Safari/537.36'
        offTheRecord: false

        userScripts: [
            WebEngineScript {
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('inject.js')
                worldId: WebEngineScript.MainWorld
            }
        ]
    }

    WebView {
        id: webview
        anchors.fill: parent

        context: webcontext
        url: 'https://habitica.com/'

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;

            if (!url.match('(http|https)://habitica.com/(.*)') && !url.match('(http|https)://(.*).stripe.(.*)/(.*)')) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    Connections {
        target: Lomiri.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('habitica.com') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
